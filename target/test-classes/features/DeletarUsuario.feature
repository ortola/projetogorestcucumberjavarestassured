#language: pt

Funcionalidade: Deletar usuários

  #Pré-condições para rodar os testes abaixo:

  #Procurar na API GET um ID já criado ou criar um ID do zero
  @deletarSucesso @deletar @regressivo
  Cenário: 1- deletar usuário específico
    Dado que eu delete um usuário
    E que informe o id 43
    Quando submeto a requisição para a api de deletar
    Então a API retorna status Sucesso na api de Exclusão com statuscode 204

  #Essa massa já não existe mais no banco de dados, sendo assim, conseguimos rodar sempre com ela este cenário
  @deletar @regressivo
  Cenário: 1- deletar usuário inexistente (404 Not Found)
    Dado que eu delete um usuário
    E que informe o id 2169
    Quando submeto a requisição para a api de deletar
    Então a API retorna status Rejeitada na api de Exclusão com statuscode 404
