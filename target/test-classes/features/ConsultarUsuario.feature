#language: pt

Funcionalidade: Consultar dados do usuário

  @consultar @regressivo
  Esquema do Cenário: <cenario>
    Dado que eu consulte um usuario
    E que informe o id <id>
    Quando submeto a requisição para a api de Consultar
    Então a API retorna status Sucesso na api de Consulta com statuscode 200
    E deve retornar o nome igual a <nome>
    E deve retornar o email igual a <email>
    E deve retornar o gender igual a <gender>
    E deve retornar o status igual a <status>
    Exemplos:
      |cenario                       |id |nome               |email                                 |gender  |status  |
      |Consultar Usuário com sucesso |48 |Rev. Dhanvin Khatri|khatri_dhanvin_rev@weimann-wunsch.biz |female  |inactive|

  @consultar @regressivo
  Cenário: Usuário não encontrado - 404 NOT Found
    Dado que eu consulte um usuario
    E que informe o id 1820
    Quando submeto a requisição para a api de Consultar
    Então a API retorna status Rejeitada na api de Consulta com statuscode 404
