#language: pt

Funcionalidade: Alterar dados do usuário

  @alterar
  Esquema do Cenário: <cenario>
    Dado que eu altere um usuário
    E informe o id "<id>"
    E que informe um nome "<nome>"
    #E que informe um email "<email>"
    #E que informe um gender "<gender>"
    #E que informe um status "<status>"
    Quando submeto a requisição para a api de Alterar
    Então a API retorna status Sucesso
    Exemplos:
      |cenario   |id|nome  |email        |gender|status|
      #|Cenario 1 |Rafael|abc26@rafe.com|male  |active|
      |1- utilizando email fixo |1820|Rafael altera|aaa120221111111112adada@aa.com|male  |active|