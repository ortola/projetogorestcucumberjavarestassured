#language: pt

Funcionalidade: Cadastro de usuários

  @cadastroEmailManual
  Esquema do Cenário: <cenario>
    Dado que eu cadastre um novo usuário
    E que informe um nome "<nome>"
    E que informe um email "<email>"
    E que informe um gender "<gender>"
    E que informe um status "<status>"
    Quando submeto a requisição para a api de Cadastrar
    Então a API retorna status Sucesso na api de Cadastro com statuscode 201
    Exemplos:
      |cenario   |nome  |email        |gender|status|
      #|Cenario 1 |Rafael|abc26@rafe.com|male  |active|
      |1- utilizando email fixo |Rafael|bvv@hotmail.com|male  |active|

  @cadastroEmailRandomico @regressivo
  Esquema do Cenário: "<cenario>"
    Dado que eu cadastre um novo usuário
    E que informe um nome "<nome>"
    E que uso um ema auto
    E que informe um gender "<gender>"
    E que informe um status "<status>"
    Quando submeto a requisição para a api de Cadastrar
    Então a API retorna status Sucesso na api de Cadastro com statuscode 201
    Exemplos:
      |cenario   |nome  |gender|status|
      |1- utilizando email randômico |Rafael|male  |active|


  @insucesso @regressivo
  Esquema do Cenário: <cenario>
    Dado que eu cadastre um novo usuário
    E que informe um nome "<nome>"
    E que informe um email "<email>"
    E que informe um gender "<gender>"
    E que informe um status "<status>"
    Quando submeto a requisição para a api de Cadastrar
    Então a API retorna status Rejeitada na api de Cadastro com statuscode 422
    E deve exibir a mensagem <mensagem> para o campo <campo>
    Exemplos:
      |cenario               |nome  |email            |gender|status|mensagem                 |campo    |
      |1- Email inválido        |Rafael|aaa1202a@aa.com  |male  |active|[has already been taken] |[email]  |
      |2- Campo nome vazio      |      |aaa12023a@aa.com |male  |active|[can't be blank]         |[name]   |
      |3- Campo email vazio     |Rafael|                 |male  |active|[can't be blank]         |[email]  |
      |4- Campo gender vazio    |Rafael|dsavz2344@g.com  |      |active|[can't be blank]         |[gender] |
      |5- Campo status vazio    |Rafael|dsavz23434@g.com |male  |      |[can't be blank]         |[status] |
      |6- Campo email inválido  |Rafael|kkkkkkkk         |male  |active|[is invalid]             |[email]  |
      |7- Campo email existente |Rafael|aaa120222a@aa.com|male  |active|[has already been taken] |[email]  |
