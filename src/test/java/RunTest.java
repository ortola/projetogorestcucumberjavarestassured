import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features"},
        glue = "StepsDefinition",
        monochrome = true,
        plugin = {"html:target/cucumber-report/cucumber.html"},
        tags = "@regressivo"
)
public class RunTest {
}
