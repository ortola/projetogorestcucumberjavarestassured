package StepsDefinition;

import Json.UsuarioRequest;
import Suporte.Config;
import com.github.javafaker.Faker;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.io.IOException;

import static Enum.Endpoints.*;


public class Steps extends Config{

    private Response response;
    UsuarioRequest NewRequest = new UsuarioRequest();

    @Dado("que eu cadastre um novo usuário")
    public void cadastrarUsuario(){
    }

    @Dado("que eu delete um usuário")
    public void deletarUsuario(){
    }

    @Dado("que eu altere um usuário")
    public void alterarUsuario(){
    }

    @Dado("que eu consulte um usuario")
    public void consultarUsuario(){
    }

    @E("que informe o id (.*?)$")
    public void informarId(Integer id) throws IOException {
        if (id == null){
            Assert.fail("O ID não pode ficar nulo(a)!");
        }
        NewRequest.setId(id);
    }

    @E("que informe um nome (.*?)$")
    public void informaNome(String nome) throws IOException {
        String a = nome.replaceAll("'","");
        if (nome == null){
            Assert.fail("O Nome não pode ficar nulo(a)!");
        }
        NewRequest.setNome(a);
    }

    @E("que informe um email (.*?)$")
    public void informaEmail(String email){
        if (email == null){
            Assert.fail("O Email não pode ficar nulo(a)!");
        }
        NewRequest.setEmail(email);
    }

    @E("que uso um ema auto")
    public void informaEmailAutomatico(){
            Faker faker = new Faker();
            String emailAutomatico = faker.name().firstName();
            String emailSufixo = emailAutomatico + "@gmail.com";

            if (emailAutomatico == null){
                Assert.fail("O Email não pode ficar nulo(a)!");
            }
        NewRequest.setEmail("\"" + emailSufixo + "\"");
    }

    @E("que informe um gender (.*?)$")
    public void informaGender(String gender){
        String a = gender.replaceAll("'","");
        if (gender == null){
            Assert.fail("O Gender não pode ficar nulo(a)!");
        }
        NewRequest.setGender(a);
    }

    @E("que informe um status (.*?)$")
    public void informaStatus(String status){
        String a = status.replaceAll("'","");
        if (status == null){
            Assert.fail("O Status não pode ficar nulo(a)!");
        }
        NewRequest.setStatus(a);
    }

    @Quando("submeto a requisição para a api de (.*?)$")
    public void requisicao(String api) {
        RequestSpecification request = RestAssured.given();

        request.header("Authorization", "Bearer c63a9bc2912af9f1e89be6837c74c9e764d111fced4eac8f36a52720f696efc6")
               .header("Content-Type", "application/json");

        if (api.matches("^[Cc][Aa][Dd][Aa][Ss][Tt][Rr][Aa][Rr]$")){
            response = request.body(NewRequest.cadastrarUsuarioAPI())
                    .when().log().all()
                    .post(BaseURL+REQUESTCADASTRO.getEndpoint());
        }
        else if (api.matches("^[Aa][Ll][Tt][Ee][Rr][Aa][Rr]$")){

            String pegaId = String.valueOf(NewRequest.getId());

            response = request.body(NewRequest.alterarUsuarioAPI())
                    .when()
                    .put(BaseURL+REQUESTALTERA.getEndpoint()+pegaId);
        }
        else if (api.matches("^[Dd][Ee][Ll][Ee][Tt][Aa][Rr]$")){
            response = request
                    .when().log().all()
                    .delete(BaseURL+REQUESTDELETE.getEndpoint()+NewRequest.getId());
        }
        else if (api.matches("^[Cc][Oo][Nn][Ss][Uu][Ll][Tt][Aa][Rr]$")){
            response = request
                    .when()
                    .get(BaseURL+REQUESTCONSULTA.getEndpoint()+NewRequest.getId());
        }
        else {
            Assert.fail("Erro no endpoint!");
        }
    }

    @Então("a API retorna status (.*?) na api de (.*?) com statuscode (.*?)$")
    public void retornaAPI(String status, String api, int code){

        //API armazena o status code da api
        int StatusCode = (response.statusCode());

        //Validação dos status de SUCESSO e suas APIs
        if (status.matches("^[Ss][Uu][Cc][Ee][Ss][Ss][Oo]$")){
            if(api.matches("^[Cc][Aa][Dd][Aa][Ss][Tt][Rr][Oo]$")){
                Assert.assertEquals(code, StatusCode);
                Integer pegaId = response.body().path("data.id");
                System.out.println("Usuario cadastrado com sucesso! " + pegaId);
            }
            else if (api.matches("^[Cc][Oo][Nn][Ss][Uu][Ll][Tt][Aa]$")){
                Assert.assertEquals(code, StatusCode);
                System.out.println("Consulta realizada!");
            }
            else if (api.matches("^[Ee][Xx][Cc][Ll][Uu][Ss][AaÃã][Oo]$")) {
                Assert.assertEquals(code, StatusCode);
                System.out.println("Usuário deletado com sucesso!");
            }
            String retornaResponse = response.then()
                    .extract()
                    .response()
                    .prettyPrint();
        }

        //Validação dos status de REJEITADA e suas APIs
        else if (status.matches("^[Rr][Ee][Jj][Ee][Ii][Tt][Aa][Dd][Aa]$")){
            if(api.matches("^[Cc][Aa][Dd][Aa][Ss][Tt][Rr][Oo]$")){
                Assert.assertEquals(code, StatusCode);
                String retornaResponse = response.then()
                        .extract()
                        .response()
                        .prettyPrint();
                System.out.println("Usuário não cadastrado!");
            }
            else if(api.matches("^[Cc][Oo][Nn][Ss][Uu][Ll][Tt][Aa]$")){
                Assert.assertEquals(code, StatusCode);
                String retornaResponse = response.then()
                        .extract()
                        .response()
                        .prettyPrint();

                Integer pegaId = response.body().path("data.id");
                System.out.println("Usuário não encontrado 404 - Not Found!");
            }
            else if (api.matches("^[Ee][Xx][Cc][Ll][Uu][Ss][AaÃã][Oo]$")) {
                Assert.assertEquals(code, StatusCode);

            }
            else {
                Assert.fail("Falha na integração!");
            }
        }
        String retornaResponse = response.then()
                .extract()
                .response()
                .prettyPrint();
    }

    @E("deve exibir a mensagem (.*?) para o campo (.*?)$")
    public void retornoCodigo(String mensagem, String campo){

        String retornocampo = response.body().path("data.field").toString();
        String retornomensagem = response.body().path("data.message").toString();

        Assert.assertEquals(mensagem, retornomensagem);
        Assert.assertEquals(campo, retornocampo);
    }

    @E("deve retornar o nome igual a (.*?)$")
    public void retornoNome(String nome){
        String retornoNome = response.body().path("data.name").toString();
        Assert.assertEquals(nome, retornoNome);
        System.out.println("Nome retornado: " + retornoNome);
    }

    @E("deve retornar o email igual a (.*?)$")
    public void retornoEmail(String email){
        String retornoEmail = response.body().path("data.email").toString();
        Assert.assertEquals(email, retornoEmail);
        System.out.println("Email retornado: " + retornoEmail);
    }

    @E("deve retornar o gender igual a (.*?)$")
    public void retornoGender(String gender){
        String retornoGender = response.body().path("data.gender").toString();
        Assert.assertEquals(gender, retornoGender);
        System.out.println("Gender retornado: " + retornoGender);
    }

    @E("deve retornar o status igual a (.*?)$")
    public void retornoStatus(String status){
        String retornoStatus = response.body().path("data.status").toString();
        Assert.assertEquals(status, retornoStatus);
        System.out.println("Status retornado: " + retornoStatus);
    }
}
