package Suporte;

import io.restassured.http.ContentType;
import org.junit.Before;

public class Config {
    //Definimos a URL BASE
    public String BaseURL = "https://gorest.co.in";

    //Definimos o content type como JSON
    ContentType CONTENT_TYPE = ContentType.JSON;

    Long MAX_TIMEOUT = 50000L;
    //Configuração para antes da execução dos nossos testes.
}
