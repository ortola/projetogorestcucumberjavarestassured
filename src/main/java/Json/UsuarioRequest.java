package Json;

public class UsuarioRequest {

    public Integer id;
    private String nome;
    private String email;
    private String gender;
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String cadastrarUsuarioAPI(){
        return "{\n" +
                "\"name\": "   + this.nome + ",\n" +
                "\"email\": "  + this.email + ",\n" +
                "\"gender\": " + this.gender + ",\n" +
                "\"status\": " + this.status + "\n" +
                "}";
    }

    public String alterarUsuarioAPI(){
        return "{\n" +
                "\"id\": "     + this.id + ",\n" +
                "\"name\": "   + this.nome + ",\n" +
                "\"email\": "  + this.email + ",\n" +
                "\"gender\": " + this.gender + ",\n" +
                "\"status\": " + this.status + "\n" +
                "}";
    }
}
