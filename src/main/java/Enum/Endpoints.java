package Enum;

public enum Endpoints {
    REQUESTCADASTRO("/public/v1/users"),
    REQUESTDELETE("/public/v1/users/"),
    REQUESTALTERA("/public/v1/users/"),
    REQUESTCONSULTA("/public/v1/users/");

    private String endpoint;

    //Construtor
    Endpoints(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }
}
